import os
from dotenv import load_dotenv

def suche_helm_ordner(root):
    helm_ordner = []
    
    for root, dirs, files in os.walk(root):
        if "helm" in dirs: 
         helm_ordner.append(os.path.join(root,"helm"))
    
    return helm_ordner


def push_tgz_files(root):
    host=os.environ.get('HARBOR_HOST')
    project=os.environ.get('HARBOR_PROJECT')
    user=os.environ.get('HARBOR_USERNAME')
    pwd=os.environ.get('HARBOR_PASSWORD')
    hostname = str(host)+"/"+str(project)
    os.system("echo "+ hostname)
    os.system("echo Start Login")  
    result=os.system("helm registry login "+hostname+ " -u '"+user+"' -p '"+ pwd+"'")
   
    if result == 0:
        if host == None or project == None:
            os.system("echo Invalid Harbor Project")
            os._exit(1)
        
        for root, dirs, files in os.walk(root):
            for file in files:
                if file.endswith(".tgz"): 
                    file = os.path.join(root,file)
                    os.system("echo start pushing to ... " + "oci://" + hostname)
                    os.system("helm push "+ file + " oci://" + hostname) 
    else:
        os.system("echo Authorization for Harbor failed.")
        os._exit(1)
    os.system("helm registry logout "+hostname)   



if __name__ == "__main__":
    root = "."
    load_dotenv()
    folders = suche_helm_ordner(root)
    versionTag=None
    tag=os.environ.get("CI_COMMIT_TAG")
    branch=os.environ.get("CI_COMMIT_BRANCH")

    if tag == None: 
        if branch != None:
            versionTag=branch
    else:
        versionTag=tag
    
    if folders:
        os.system("echo Found Folders:")
        os.system("mkdir helmcharts")
        for helm_ordner in folders:
            os.system("echo "+ helm_ordner)     
            
            try:
                path = os.path.join(helm_ordner,"Chart.yaml")

                with open(path, 'r') as file:
                    content = file.read()
                    
                new_content = content.replace("-tag", "-"+versionTag)

                with open(path, 'w') as file:
                    file.write(new_content)
            except FileNotFoundError:
                os.system(f'echo Chart.yaml not found')
                os._exit(1)
            except Exception as e:
                os.system(f'echo Error: {e}')
                os._exit(1) 
                       
            os.system("helm dependency build "+helm_ordner)
            os.system("helm package "+helm_ordner +" -d helmcharts")    
        push_tgz_files("helmcharts")  
    else:
        os.system("echo No Folders found.")
